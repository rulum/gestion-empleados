import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginaPrincipalComponent } from './ui/pages/pagina-principal/pagina-principal.component';
import { NotFoundComponent } from './ui/pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'empleados',
    pathMatch: 'full'
    // component: PaginaPrincipalComponent,
  },
  {
    path: 'empleados',
    loadChildren: () =>
      import('./empleados/empleados.module').then((m) => m.EmpleadosModule),
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
