import { EmpleadoDTO, EmpleadoData } from './EmpleadoDTO';

export class Empleado {
  id: number;
  nombres: string;
  apellidos: string;
  dni: string;
  celular: string;

  constructor(data: EmpleadoData) {
    this.id = data.id;
    this.nombres = data.nombres;
    this.apellidos = data.apellidos;
    this.dni = data.dni;
    this.celular = data.celular;
  }
}