import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { merge, scan, Subject, switchMap, map, concatMap, Observable, combineLatest, tap, catchError, throwError, BehaviorSubject } from 'rxjs';
import { EmpleadoResponse, EmpleadoDTO, EmpleadoData } from './EmpleadoDTO';
import { Empleado } from './Empleado';
import { CrudOperations } from '../interfaces/General';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private http: HttpClient) { }

  private empleados$: Observable<Empleado[]> = this.http.get<EmpleadoResponse[]>(`${environment.api}/empleados`).pipe(
    map(empleado => empleado.map(it => new Empleado(it))),
  );

  private employeeCRUDSubject = new Subject<CrudOperations<EmpleadoDTO>>();
  employeeCRUDAction$ = this.employeeCRUDSubject.asObservable();

  empleadosJoined$ = merge(
    this.empleados$,
    this.employeeCRUDAction$.pipe(
      concatMap(operation => this.employeeServerOperations(operation).pipe(
        map(it => ({ action: operation.action, data: it}))
      ))
    )
  ).pipe(
    scan(this.modifyDataEmployee, [] as Empleado[]),
  );

  employeeServerOperations(operation: CrudOperations<EmpleadoDTO>): Observable<Empleado> {

    let $employee = new Observable<EmpleadoResponse>();

    if (operation.action === 'create')
      $employee = this.createEmployeeInServer(operation.data)

    if (operation.action === 'update' && operation.data.id)
      $employee = this.updateEmployeeInServer(operation.data.id, operation.data)

    if (operation.action === 'delete' && operation.data.id)
      $employee = this.deleteEmployeeInServer(operation.data.id)

    return $employee.pipe(
      map(it => new Empleado(it))
    );
  }

  modifyDataEmployee(employees: Empleado[], value: Empleado[] | CrudOperations<Empleado>): Empleado[] {

    if (value instanceof Array) return [...value]

    if (value.action === 'create')
      return [...employees, value.data]

    if (value.action === 'update')
      return employees.map(it => it.id === value.data.id ? value.data : it)

    if (value.action === 'delete')
      return employees.filter(it => it.id !== value.data.id)

    return employees
  }

  addEmpleado(empleado: EmpleadoDTO) {
    this.employeeCRUDSubject.next({ action: 'create', data: empleado });
  }

  updateEmpleado(id: number, empleado: EmpleadoDTO) {
    this.employeeCRUDSubject.next({ action: 'update', data: { ...empleado, id } });
  }

  deleteEmployee(id: number) {
    this.employeeCRUDSubject.next({ action: 'delete', data: { id }})
  }

  selectEmpleadoById(id: number) {
    this.empleadoSelectedIdSubject.next(id);
  }

  private empleadoSelectedIdSubject = new BehaviorSubject<number>(0);
  empleadoSelectedIdAction$ = this.empleadoSelectedIdSubject.asObservable();

  empleado$ = combineLatest([
    this.empleados$,
    this.empleadoSelectedIdAction$
  ]).pipe(
    map(([empleados, empleadoSelectedId]) => empleados.find(it => it.id === empleadoSelectedId)),
    catchError((e: Error) => throwError(() => {
      return 'unknown error please try again'
    }))
  );

  getEmpleadoById(id: number) {
    return this.http.get<EmpleadoResponse>(`${environment.api}/empleados/${id}`)
      .pipe(
        map(it => new Empleado(it))
      )
  }

  createEmployeeInServer(empleado: EmpleadoDTO) {
    return this.http.post<EmpleadoResponse>(`${environment.api}/empleados`, empleado)
  }

  updateEmployeeInServer(id: number, empleado: EmpleadoDTO) {
    return this.http.put<EmpleadoResponse>(`${environment.api}/empleados/${id}`, empleado)
      .pipe(
        map(it => new Empleado(it))
      )
  }

  deleteEmployeeInServer(id: number) {
    return this.http.delete<EmpleadoResponse>(`${environment.api}/empleados/${id}`)
      .pipe(
        map(it => new Empleado({...it, id}))
      )
  }

}
