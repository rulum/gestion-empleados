import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private _loadingGet = new BehaviorSubject<boolean>(false);
  private _loadingPost = new BehaviorSubject<boolean>(false);
  private _loadingDelete = new BehaviorSubject<boolean>(false);

  constructor() { }

  activarGet() { this._loadingGet.next(true) }

  desactivarGet() { this._loadingGet.next(false) }

  activarPost() { this._loadingPost.next(true) }

  desactivarPost() { this._loadingPost.next(false) }

  activarDelete() { this._loadingDelete.next(true) }

  desactivarDelete() { this._loadingDelete.next(false) }

  get loadingGet$() { return this._loadingGet.asObservable() }

  get loadingPost$() { return this._loadingPost.asObservable() }

  get loadingDelete$() { return this._loadingDelete.asObservable() }
}
