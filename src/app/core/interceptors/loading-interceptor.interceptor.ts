import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { finalize, Observable } from 'rxjs';
import { LoadingService } from '../general/loading.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  totalRequestsGet = 0;
  completedRequestsGet = 0;

  totalRequestsPost = 0;
  completedRequestsPost = 0;

  totalRequestsDelete = 0;
  completedRequestsDelete = 0;

  constructor( private loadingService: LoadingService ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const isGet = request.method === 'GET'
    const isAddOrUpdate = request.method === 'POST' || request.method === 'PUT' || request.method === 'PATCH'
    const isDelete = request.method === 'DELETE'

    if (isGet) {
      this.loadingService.activarGet();
      this.totalRequestsGet++;
    }

    if (isAddOrUpdate) {
      this.loadingService.activarPost();
      this.totalRequestsPost++;
    }

    if (isDelete) {
      this.loadingService.activarDelete();
      this.totalRequestsDelete++;
    }

    return next.handle(request).pipe(
      finalize(() => {
        if (isGet) {
          this.completedRequestsGet++;
          if (this.completedRequestsGet === this.totalRequestsGet) {
            this.loadingService.desactivarGet();
            this.completedRequestsGet = 0;
            this.totalRequestsGet = 0;
          }
        }

        if (isAddOrUpdate) {
          this.completedRequestsPost++;
          if (this.completedRequestsPost === this.totalRequestsPost) {
            this.loadingService.desactivarPost();
            this.completedRequestsPost = 0;
            this.totalRequestsPost = 0;
          }
        }

        if (isDelete) {
          this.completedRequestsDelete++;
          if (this.completedRequestsDelete === this.totalRequestsDelete) {
            this.loadingService.desactivarDelete();
            this.completedRequestsDelete = 0;
            this.totalRequestsDelete = 0;
          }
        }
      })
    );
  }
}
