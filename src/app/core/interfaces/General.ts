export interface CrudOperations<T> {
  action: CrudOperationsType;
  data: T
}

export type CrudOperationsType = 'create' | 'read' | 'update' | 'delete'
