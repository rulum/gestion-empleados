import { Component } from '@angular/core';
import { EmpleadoService } from '../../../core/empleado/empleado.service';
import { FormBuilder, Validators } from '@angular/forms';
import { EmpleadoDTO } from '../../../core/empleado/EmpleadoDTO';
import { LoadingService } from '../../../core/general/loading.service';

@Component({
  selector: 'app-formulario-empleado',
  templateUrl: './formulario-empleado.component.html',
  styleUrls: ['./formulario-empleado.component.css'],
})
export class FormularioEmpleadoComponent {

  constructor(
    private empleadoService: EmpleadoService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
  ) {}

  loading$ = this.loadingService.loadingPost$;

  formularioEmpleado = this.fb.group({
    nombres: ['', Validators.required],
    apellidos: ['', Validators.required],
    dni: ['', Validators.required],
    celular: ['', Validators.required]
  })

  guardarEmpleado() {
    if (this.formularioEmpleado.invalid) {
      this.formularioEmpleado.markAllAsTouched();
      return;
    }
    this.empleadoService.addEmpleado(this.formularioEmpleado.value as EmpleadoDTO);
    this.formularioEmpleado.reset();
  }
}
