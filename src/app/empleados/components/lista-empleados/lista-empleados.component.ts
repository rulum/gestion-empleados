import { Component } from '@angular/core';
import { EmpleadoService } from '../../../core/empleado/empleado.service';
import { faArrowRightToBracket, faPenToSquare, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-lista-empleados',
  templateUrl: './lista-empleados.component.html',
  styleUrls: ['./lista-empleados.component.css'],
})
export class ListaEmpleadosComponent {
  constructor(private empleadoService: EmpleadoService) {}

  empleados$ = this.empleadoService.empleadosJoined$;

  faArrowRightToBracket = faArrowRightToBracket;

  faPenToSquare = faPenToSquare;

  faTrash = faTrash

  deleteAction(id: number) {
    this.empleadoService.deleteEmployee(id)
  }
}
