import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoComponent } from './pages/listado/listado.component';
import { DetalleComponent } from './pages/detalle/detalle.component';
import { PrincipalComponent } from './pages/principal/principal.component';
import { EditarComponent } from './pages/editar/editar.component';

const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: ListadoComponent
      },
      {
        path: ':id',
        pathMatch: 'full',
        component: DetalleComponent
      },
      {
        path: 'editar/:id',
        pathMatch: 'full',
        component: EditarComponent
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmpleadosRoutingModule {}
