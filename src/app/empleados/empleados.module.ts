import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { ListadoComponent } from './pages/listado/listado.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DetalleComponent } from './pages/detalle/detalle.component';
import { UiModule } from '../ui/ui.module';
import { PrincipalComponent } from './pages/principal/principal.component';
import { FormularioEmpleadoComponent } from './components/formulario-empleado/formulario-empleado.component';
import { ListaEmpleadosComponent } from './components/lista-empleados/lista-empleados.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EditarComponent } from './pages/editar/editar.component';

@NgModule({
  declarations: [
    ListadoComponent,
    DetalleComponent,
    PrincipalComponent,
    FormularioEmpleadoComponent,
    ListaEmpleadosComponent,
    EditarComponent,
  ],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    ReactiveFormsModule,
    UiModule,
    FontAwesomeModule,
  ],
})
export class EmpleadosModule {}
