import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from '../../../core/empleado/empleado.service';
import { delay, Observable, switchMap } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from '../../../core/empleado/Empleado';
import { LoadingService } from '../../../core/general/loading.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
})
export class DetalleComponent implements OnInit {

  constructor(
    private empleadoService: EmpleadoService,
    private loadingService: LoadingService,
    private route: ActivatedRoute
  ) {}

  loading$: Observable<boolean> = this.loadingService.loadingGet$;

  empleado$!: Observable<Empleado>;
  selectedId: number = 0;

  ngOnInit(): void {
    this.empleado$ = this.route.paramMap.pipe(
      delay(0),
      switchMap(params => {
        this.selectedId = Number(params.get('id'));
        return this.empleadoService.getEmpleadoById(this.selectedId);
      })
    )
  }

}
