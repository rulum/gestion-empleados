import { Component } from '@angular/core';
import { EmpleadoService } from '../../../core/empleado/empleado.service';
import { LoadingService } from '../../../core/general/loading.service';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from '../../../core/empleado/Empleado';
import { Observable, switchMap, tap, delay } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { EmpleadoDTO } from '../../../core/empleado/EmpleadoDTO';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css'],
})
export class EditarComponent {

  constructor(
    private empleadoService: EmpleadoService,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
  ) {}

  loading$ = this.loadingService.loadingGet$;
  loadingEdit$ = this.loadingService.loadingPost$;

  empleado$!: Observable<Empleado>;
  selectedId: number = 0;

  formularioEmpleado = this.fb.group({
    nombres: ['', Validators.required],
    apellidos: ['', Validators.required],
    dni: ['', Validators.required],
    celular: ['', Validators.required]
  })

  ngOnInit(): void {
    this.empleado$ = this.route.paramMap.pipe(
      delay(0),
      switchMap(params => {
        this.selectedId = Number(params.get('id'));
        return this.empleadoService.getEmpleadoById(this.selectedId).pipe(
          tap(it => {
            this.formularioEmpleado.setValue({ 
              nombres: it.nombres, 
              apellidos: it.apellidos,
              dni: it.dni,
              celular: it.celular,
            })
          })
        );
      })
    )
  }

  editarEmpleado() {
    if (this.formularioEmpleado.invalid) {
      this.formularioEmpleado.markAllAsTouched();
      return;
    }
    this.empleadoService.updateEmployeeInServer(this.selectedId, this.formularioEmpleado.value as EmpleadoDTO).subscribe();
  }
}
