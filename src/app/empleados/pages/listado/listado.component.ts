import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { combineLatest, map, tap } from 'rxjs';
import { EmpleadoService } from '../../../core/empleado/empleado.service';
import { EmpleadoDTO } from '../../../core/empleado/EmpleadoDTO';
import { LoadingService } from '../../../core/general/loading.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
})
export class ListadoComponent {

  constructor(
    private empleadoService: EmpleadoService,
    private loadingService: LoadingService,
    private router: Router
  ) {}

  loadingDelete$ = this.loadingService.loadingDelete$;

  vm$ = combineLatest([
    this.empleadoService.empleadosJoined$,
    this.empleadoService.empleado$,
  ]).pipe(
    map(([empleados, empleado]) => ({ empleados, empleado })),
  )

  verDetalle(id: number) {
    this.empleadoService.selectEmpleadoById(id);
    this.router.navigate(['empleados/detalle']);
  }

}
