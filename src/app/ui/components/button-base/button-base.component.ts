import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button-base',
  templateUrl: './button-base.component.html',
  styleUrls: [],
})
export class ButtonBaseComponent {
  @Input() label: string = '';
  @Input() type: string = '';
}
