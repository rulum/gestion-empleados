import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-base',
  templateUrl: './card-base.component.html',
  styleUrls: ['./card-base.component.css'],
})
export class CardBaseComponent {}
