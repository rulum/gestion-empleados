import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-base',
  templateUrl: './input-base.component.html',
  styleUrls: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputBaseComponent,
      multi: true
    }
  ],
})
export class InputBaseComponent implements ControlValueAccessor {

  @Input() label: string = ''
  @Input() id: string = ''

  private _inputText: string = '';

  private propagateChange = (value: string) => {};

  writeValue(value: string): void {
    if (value != null) {
      this._inputText = value;
      return;
    }
    this._inputText = '';
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {}
  setDisabledState?(isDisabled: boolean): void {}

  cambiaInputText(e: Event) {
    this.inputText = (e.target as HTMLInputElement).value
  }

  get tieneTexto() {
    return this._inputText.length > 0;
  }

  get inputText() {
    return this._inputText;
  }

  set inputText(value: string) {
    this._inputText = value;
    this.propagateChange(value);
  }

}
