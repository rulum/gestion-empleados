import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarButtonBaseComponent } from './navbar-button-base.component';

describe('NavbarButtonBaseComponent', () => {
  let component: NavbarButtonBaseComponent;
  let fixture: ComponentFixture<NavbarButtonBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NavbarButtonBaseComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NavbarButtonBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
