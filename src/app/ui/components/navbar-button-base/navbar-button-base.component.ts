import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-button-base',
  templateUrl: './navbar-button-base.component.html',
  styleUrls: ['./navbar-button-base.component.css'],
})
export class NavbarButtonBaseComponent {
  @Input() text: string = '';
  @Input() goRoute: string = '';

  constructor(private router: Router) {}

  goToPage() {
    this.router.navigate([this.goRoute]);
  }
}
