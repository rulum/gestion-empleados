import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFieldBaseComponent } from './text-field-base.component';

describe('TextFieldBaseComponent', () => {
  let component: TextFieldBaseComponent;
  let fixture: ComponentFixture<TextFieldBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextFieldBaseComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TextFieldBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
