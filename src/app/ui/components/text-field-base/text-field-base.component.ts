import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-text-field-base',
  templateUrl: './text-field-base.component.html',
  styleUrls: ['./text-field-base.component.css'],
})
export class TextFieldBaseComponent {
  @Input() label: string = ''
  @Input() value: string = ''
}
