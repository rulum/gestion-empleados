import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginaPrincipalComponent } from './pages/pagina-principal/pagina-principal.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { InputBaseComponent } from './components/input-base/input-base.component';
import { ButtonBaseComponent } from './components/button-base/button-base.component';
import { NavbarBaseComponent } from './components/navbar-base/navbar-base.component';
import { NavbarButtonBaseComponent } from './components/navbar-button-base/navbar-button-base.component';
import { CardBaseComponent } from './components/card-base/card-base.component';
import { TextFieldBaseComponent } from './components/text-field-base/text-field-base.component';

@NgModule({
  declarations: [
    PaginaPrincipalComponent,
    NotFoundComponent,
    InputBaseComponent,
    ButtonBaseComponent,
    NavbarBaseComponent,
    NavbarButtonBaseComponent,
    CardBaseComponent,
    TextFieldBaseComponent,
  ],
  exports: [
    PaginaPrincipalComponent,
    InputBaseComponent,
    ButtonBaseComponent,
    NavbarBaseComponent,
    CardBaseComponent,
    TextFieldBaseComponent,
  ],
  imports: [CommonModule],
})
export class UiModule {}
